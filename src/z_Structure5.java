import java.util.Random;

public class z_Structure5 implements I_StructureExpFctns {
    private Random r = new Random();

    @Override
    public void elseif_int() {
        int v = r.nextInt(6);

        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < 100000000; ++i) {
            if (v == 0) {
                v = r.nextInt(6);
            } else if (v == 1) {
                v = r.nextInt(6);
            } else if (v == 2) {
                v = r.nextInt(6);
            } else if (v == 3) {
                v = r.nextInt(6);
            } else if (v == 4) {
                v = r.nextInt(6);
            } else {
                v = r.nextInt(6);
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void elseif_str() {
        String v = r.nextInt(6) + " ";

        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < 100000000; ++i) {
            if (v.equals("0 ")) {
                v = r.nextInt(6) + " ";
            } else if (v.equals("1 ")) {
                v = r.nextInt(6) + " ";
            } else if (v.equals("2 ")) {
                v = r.nextInt(6) + " ";
            } else if (v.equals("3 ")) {
                v = r.nextInt(6) + " ";
            } else if (v.equals("4 ")) {
                v = r.nextInt(6) + " ";
            } else {
                v = r.nextInt(6) + " ";
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void elseif_enm() {
        HelperFctns.Type6 t = HelperFctns.nextType6();

        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < 100000000; ++i) {
            if (t == HelperFctns.Type6.a) {
                t = HelperFctns.nextType6();
            } else if (t == HelperFctns.Type6.b) {
                t = HelperFctns.nextType6();
            } else if (t == HelperFctns.Type6.c) {
                t = HelperFctns.nextType6();
            } else if (t == HelperFctns.Type6.d) {
                t = HelperFctns.nextType6();
            } else if (t == HelperFctns.Type6.e) {
                t = HelperFctns.nextType6();
            } else {
                t = HelperFctns.nextType6();
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void switch_int() {
        int v = r.nextInt(6);
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < 100000000; ++i) {
            switch (v) {
                case 0:
                    v = r.nextInt(6);
                    break;
                case 1:
                    v = r.nextInt(6);
                    break;
                case 2:
                    v = r.nextInt(6);
                    break;
                case 3:
                    v = r.nextInt(6);
                    break;
                case 4:
                    v = r.nextInt(6);
                    break;
                default:
                    v = r.nextInt(6);
                    break;
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }


    @Override
    public void switch_str() {
        String v = r.nextInt(6) + " ";
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < 100000000; ++i) {
            switch (v) {
                case "0 ":
                    v = r.nextInt(6) + " ";
                    break;
                case "1 ":
                    v = r.nextInt(6) + " ";
                    break;
                case "2 ":
                    v = r.nextInt(6) + " ";
                    break;
                case "3 ":
                    v = r.nextInt(6) + " ";
                    break;
                case "4 ":
                    v = r.nextInt(6) + " ";
                    break;
                default:
                    v = r.nextInt(6) + " ";
                    break;
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void switch_enm() {
        HelperFctns.Type6 t = HelperFctns.nextType6();

        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < 100000000; ++i) {
            switch (t) {
                case a:
                    t = HelperFctns.nextType6();
                    break;
                case b:
                    t = HelperFctns.nextType6();
                    break;
                case c:
                    t = HelperFctns.nextType6();
                    break;
                case d:
                    t = HelperFctns.nextType6();
                    break;
                case e:
                    t = HelperFctns.nextType6();
                    break;
                default:
                    t = HelperFctns.nextType6();
                    break;
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }
}
