public interface I_StructureExpFctns { //structure experiment functions
    void elseif_int();

    void elseif_str();

    void elseif_enm();

    void switch_int();

    void switch_str();

    void switch_enm();
}
