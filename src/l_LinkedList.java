import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class l_LinkedList implements I_CollectionExpFctns {
    Random r = new Random();

    @Override
    public void insert_int(int num, int repeats) {
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            List<Integer> c = new LinkedList<>();
            for (int j = 0; j < num; ++j) {
                c.add(r.nextInt()); //random numbers to prevent optimisation
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void insert_dbl(int num, int repeats) {
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            List<Double> c = new LinkedList<>();
            for (int j = 0; j < num; ++j) {
                c.add(r.nextDouble());
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void insert_str(int num, int repeats) {
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            List<String> c = new LinkedList<>();
            for (int j = 0; j < num; ++j) {
                c.add(HelperFctns.nextString());
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void iterate_loop_int(int num, int repeats) {
        List<Integer> c = new LinkedList<>();
        for (int j = 0; j < num; ++j) {
            c.add(r.nextInt());
        }
        int x;
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            for (int j = 0; j < c.size(); ++j) {
                x = c.get(j);
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void iterate_loop_dbl(int num, int repeats) {
        List<Double> c = new LinkedList<>();
        for (int j = 0; j < num; ++j) {
            c.add(r.nextDouble());
        }
        double x;
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            for (int j = 0; j < c.size(); ++j) {
                x = c.get(j);
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void iterate_loop_str(int num, int repeats) {
        List<String> c = new LinkedList<>();
        for (int j = 0; j < num; ++j) {
            c.add(HelperFctns.nextString());
        }
        String x;
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            for (int j = 0; j < c.size(); ++j) {
                x = c.get(j);
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void iterate_iter_int(int num, int repeats) {
        List<Integer> c = new LinkedList<>();
        for (int j = 0; j < num; ++j) {
            c.add(r.nextInt());
        }
        int x;
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            for (int e : c) {
                x = e;
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void iterate_iter_dbl(int num, int repeats) {
        List<Double> c = new LinkedList<>();
        for (int j = 0; j < num; ++j) {
            c.add(r.nextDouble());
        }
        double x;
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            for (double e : c) {
                x = e;
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void iterate_iter_str(int num, int repeats) {
        List<String> c = new LinkedList<>();
        for (int j = 0; j < num; ++j) {
            c.add(HelperFctns.nextString());
        }
        String x;
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            for (String e : c) {
                x = e;
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void randr_int(int num, int repeats) {
        List<Integer> c = new LinkedList<>();
        for (int j = 0; j < num; ++j) {
            c.add(r.nextInt());
        }
        int x;
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            x = c.get(r.nextInt(num));
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    public void randr_dbl(int num, int repeats) {
        List<Double> c = new LinkedList<>();
        for (int j = 0; j < num; ++j) {
            c.add(r.nextDouble());
        }
        double x;
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            x = c.get(r.nextInt(num));
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void randr_str(int num, int repeats) {
        List<String> c = new LinkedList<>();
        for (int j = 0; j < num; ++j) {
            c.add(HelperFctns.nextString());
        }
        String x;
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            x = c.get(r.nextInt(num));
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void randw_int(int num, int repeats) {
        List<Integer> c = new LinkedList<>();
        for (int j = 0; j < num; ++j) {
            c.add(r.nextInt());
        }
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            c.set(r.nextInt(num), r.nextInt());
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void randw_dbl(int num, int repeats) {
        List<Double> c = new LinkedList<>();
        for (int j = 0; j < num; ++j) {
            c.add(r.nextDouble());
        }
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            c.set(r.nextInt(num), r.nextDouble());
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void randw_str(int num, int repeats) {
        List<String> c = new LinkedList<>();
        for (int j = 0; j < num; ++j) {
            c.add(HelperFctns.nextString());
        }
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            c.set(r.nextInt(num), HelperFctns.nextString());
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void contains_int(int num, int repeats) {
        List<Integer> c = new LinkedList<>();
        for (int j = 0; j < num; ++j) {
            c.add(r.nextInt());
        }
        HelperFctns.setContainsInt(c);
        boolean x;
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            x = c.contains(HelperFctns.nextContainsInt());
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void contains_dbl(int num, int repeats) {
        List<Double> c = new LinkedList<>();
        for (int j = 0; j < num; ++j) {
            c.add(r.nextDouble());
        }
        HelperFctns.setContainsDbl(c);
        boolean x;
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            x = c.contains(HelperFctns.nextContainsDbl());
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void contains_str(int num, int repeats) {
        List<String> c = new LinkedList<>();
        for (int j = 0; j < num; ++j) {
            c.add(HelperFctns.nextString());
        }
        HelperFctns.setContainsStr(c);
        boolean x;
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            x = c.contains(HelperFctns.nextContainsStr());
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }
}
