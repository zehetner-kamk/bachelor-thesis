package database;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ResultDao {
    private static final String TABLE_NAME = "r";
    /*
CREATE TABLE r(id INTEGER PRIMARY KEY AUTOINCREMENT,
coll VARCHAR(50) NOT NULL,
impl VARCHAR(50) NOT NULL,
method VARCHAR(50) NOT NULL,
datatype VARCHAR(50) NOT NULL,
num INT NOT NULL,
iteration INT NOT NULL,
duration DOUBLE NOT NULL,
power DOUBLE NOT NULL);
*/

    public boolean insert(ResultDO result) {
        String insertStatement = "INSERT INTO " + TABLE_NAME + " (coll, impl, method, datatype, num, iteration, duration, power) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

        if (ConnectionManager.getConnection() != null) {
            try (PreparedStatement statement = ConnectionManager.getConnection().prepareStatement(insertStatement)) {
                statement.setString(1, result.collection);
                statement.setString(2, result.implementation);
                statement.setString(3, result.method);
                statement.setString(4, result.datatype);
                statement.setInt(5, result.numOfElements);
                statement.setInt(6,result.iteration);
                statement.setDouble(7, result.duration);
                statement.setDouble(8, result.power);

                statement.execute();
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
