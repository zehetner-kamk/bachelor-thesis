package database;

public class ResultDO {
    public String collection;
    public String implementation;
    public String method;
    public String datatype;
    public int numOfElements;
    public int iteration;
    public double duration;
    public double power;

    public ResultDO(String collection, String implementation, String method, String datatype, int numOfElements, int iteration, double duration, double power) {
        this.collection = collection;
        this.implementation = implementation;
        this.method = method;
        this.datatype = datatype;
        this.numOfElements = numOfElements;
        this.iteration = iteration;
        this.duration = duration;
        this.power = power;
    }
}
