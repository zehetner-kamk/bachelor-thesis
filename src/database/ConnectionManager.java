package database;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Scanner;

public class ConnectionManager {
    private static Connection connection;


    public static Connection getConnection() {
        try {
            if (connection == null || connection.isClosed()) { //the db url couldn't be retrieved from a file, therefore it needs to be adapted here
                connection = DriverManager.getConnection("jdbc:sqlite:results_new.db");
            }
            return connection;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null; //could not create connection
    }

    private static String readConfig(String filename) {
        try {
            Scanner s = new Scanner(new File(filename));
            return s.nextLine();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }
}
