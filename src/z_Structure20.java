import java.util.Random;

public class z_Structure20 implements I_StructureExpFctns {
    private Random r = new Random();

    @Override
    public void elseif_int() {
        int v = r.nextInt(21);

        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < 100000000; ++i) {
            if (v == 0) {
                v = r.nextInt(21);
            } else if (v == 1) {
                v = r.nextInt(21);
            } else if (v == 2) {
                v = r.nextInt(21);
            } else if (v == 3) {
                v = r.nextInt(21);
            } else if (v == 4) {
                v = r.nextInt(21);
            } else if (v == 5) {
                v = r.nextInt(21);
            } else if (v == 6) {
                v = r.nextInt(21);
            } else if (v == 7) {
                v = r.nextInt(21);
            } else if (v == 8) {
                v = r.nextInt(21);
            } else if (v == 9) {
                v = r.nextInt(21);
            } else if (v == 10) {
                v = r.nextInt(21);
            } else if (v == 11) {
                v = r.nextInt(21);
            } else if (v == 12) {
                v = r.nextInt(21);
            } else if (v == 13) {
                v = r.nextInt(21);
            } else if (v == 14) {
                v = r.nextInt(21);
            } else if (v == 15) {
                v = r.nextInt(21);
            } else if (v == 16) {
                v = r.nextInt(21);
            } else if (v == 17) {
                v = r.nextInt(21);
            } else if (v == 18) {
                v = r.nextInt(21);
            } else if (v == 19) {
                v = r.nextInt(21);
            } else {
                v = r.nextInt(21);
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void elseif_str() {
        String v = r.nextInt(21) + " ";

        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < 100000000; ++i) {
            if (v.equals("0 ")) {
                v = r.nextInt(21) + " ";
            } else if (v.equals("1 ")) {
                v = r.nextInt(21) + " ";
            } else if (v.equals("2 ")) {
                v = r.nextInt(21) + " ";
            } else if (v.equals("3 ")) {
                v = r.nextInt(21) + " ";
            } else if (v.equals("4 ")) {
                v = r.nextInt(21) + " ";
            } else if (v.equals("5 ")) {
                v = r.nextInt(21) + " ";
            } else if (v.equals("6 ")) {
                v = r.nextInt(21) + " ";
            } else if (v.equals("7 ")) {
                v = r.nextInt(21) + " ";
            } else if (v.equals("8 ")) {
                v = r.nextInt(21) + " ";
            } else if (v.equals("9 ")) {
                v = r.nextInt(21) + " ";
            } else if (v.equals("10 ")) {
                v = r.nextInt(21) + " ";
            } else if (v.equals("11 ")) {
                v = r.nextInt(21) + " ";
            } else if (v.equals("12 ")) {
                v = r.nextInt(21) + " ";
            } else if (v.equals("13 ")) {
                v = r.nextInt(21) + " ";
            } else if (v.equals("14 ")) {
                v = r.nextInt(21) + " ";
            } else if (v.equals("15 ")) {
                v = r.nextInt(21) + " ";
            } else if (v.equals("16 ")) {
                v = r.nextInt(21) + " ";
            } else if (v.equals("17 ")) {
                v = r.nextInt(21) + " ";
            } else if (v.equals("18 ")) {
                v = r.nextInt(21) + " ";
            } else if (v.equals("19 ")) {
                v = r.nextInt(21) + " ";
            } else {
                v = r.nextInt(21) + " ";
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void elseif_enm() {
        HelperFctns.Type21 t = HelperFctns.nextType21();

        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < 100000000; ++i) {
            if (t == HelperFctns.Type21.a) {
                t = HelperFctns.nextType21();
            } else if (t == HelperFctns.Type21.b) {
                t = HelperFctns.nextType21();
            } else if (t == HelperFctns.Type21.c) {
                t = HelperFctns.nextType21();
            } else if (t == HelperFctns.Type21.d) {
                t = HelperFctns.nextType21();
            } else if (t == HelperFctns.Type21.e) {
                t = HelperFctns.nextType21();
            } else if (t == HelperFctns.Type21.f) {
                t = HelperFctns.nextType21();
            } else if (t == HelperFctns.Type21.g) {
                t = HelperFctns.nextType21();
            } else if (t == HelperFctns.Type21.h) {
                t = HelperFctns.nextType21();
            } else if (t == HelperFctns.Type21.i) {
                t = HelperFctns.nextType21();
            } else if (t == HelperFctns.Type21.j) {
                t = HelperFctns.nextType21();
            } else if (t == HelperFctns.Type21.k) {
                t = HelperFctns.nextType21();
            } else if (t == HelperFctns.Type21.l) {
                t = HelperFctns.nextType21();
            } else if (t == HelperFctns.Type21.m) {
                t = HelperFctns.nextType21();
            } else if (t == HelperFctns.Type21.n) {
                t = HelperFctns.nextType21();
            } else if (t == HelperFctns.Type21.o) {
                t = HelperFctns.nextType21();
            } else if (t == HelperFctns.Type21.p) {
                t = HelperFctns.nextType21();
            } else if (t == HelperFctns.Type21.q) {
                t = HelperFctns.nextType21();
            } else if (t == HelperFctns.Type21.r) {
                t = HelperFctns.nextType21();
            } else if (t == HelperFctns.Type21.s) {
                t = HelperFctns.nextType21();
            } else if (t == HelperFctns.Type21.t) {
                t = HelperFctns.nextType21();
            } else if (t == HelperFctns.Type21.u) {
                t = HelperFctns.nextType21();
            } else {
                t = HelperFctns.nextType21();
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void switch_int() {
        int v = r.nextInt(21);
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < 100000000; ++i) {
            switch (v) {
                case 0:
                    v = r.nextInt(21);
                    break;
                case 1:
                    v = r.nextInt(21);
                    break;
                case 2:
                    v = r.nextInt(21);
                    break;
                case 3:
                    v = r.nextInt(21);
                    break;
                case 4:
                    v = r.nextInt(21);
                    break;
                case 5:
                    v = r.nextInt(21);
                    break;
                case 6:
                    v = r.nextInt(21);
                    break;
                case 7:
                    v = r.nextInt(21);
                    break;
                case 8:
                    v = r.nextInt(21);
                    break;
                case 9:
                    v = r.nextInt(21);
                    break;
                case 10:
                    v = r.nextInt(21);
                    break;
                case 11:
                    v = r.nextInt(21);
                    break;
                case 12:
                    v = r.nextInt(21);
                    break;
                case 13:
                    v = r.nextInt(21);
                    break;
                case 14:
                    v = r.nextInt(21);
                    break;
                case 15:
                    v = r.nextInt(21);
                    break;
                case 16:
                    v = r.nextInt(21);
                    break;
                case 17:
                    v = r.nextInt(21);
                    break;
                case 18:
                    v = r.nextInt(21);
                    break;
                case 19:
                    v = r.nextInt(21);
                    break;
                default:
                    v = r.nextInt(21);
                    break;
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }


    @Override
    public void switch_str() {
        String v = r.nextInt(21) + " ";
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < 100000000; ++i) {
            switch (v) {
                case "0 ":
                    v = r.nextInt(21) + " ";
                    break;
                case "1 ":
                    v = r.nextInt(21) + " ";
                    break;
                case "2 ":
                    v = r.nextInt(21) + " ";
                    break;
                case "3 ":
                    v = r.nextInt(21) + " ";
                    break;
                case "4 ":
                    v = r.nextInt(21) + " ";
                    break;
                case "5 ":
                    v = r.nextInt(21) + " ";
                    break;
                case "6 ":
                    v = r.nextInt(21) + " ";
                    break;
                case "7 ":
                    v = r.nextInt(21) + " ";
                    break;
                case "8 ":
                    v = r.nextInt(21) + " ";
                    break;
                case "9 ":
                    v = r.nextInt(21) + " ";
                    break;
                case "10 ":
                    v = r.nextInt(21) + " ";
                    break;
                case "11 ":
                    v = r.nextInt(21) + " ";
                    break;
                case "12 ":
                    v = r.nextInt(21) + " ";
                    break;
                case "13 ":
                    v = r.nextInt(21) + " ";
                    break;
                case "14 ":
                    v = r.nextInt(21) + " ";
                    break;
                case "15 ":
                    v = r.nextInt(21) + " ";
                    break;
                case "16 ":
                    v = r.nextInt(21) + " ";
                    break;
                case "17 ":
                    v = r.nextInt(21) + " ";
                    break;
                case "18 ":
                    v = r.nextInt(21) + " ";
                    break;
                case "19 ":
                    v = r.nextInt(21) + " ";
                    break;
                default:
                    v = r.nextInt(21) + " ";
                    break;
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void switch_enm() {
        HelperFctns.Type21 t = HelperFctns.nextType21();

        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < 100000000; ++i) {
            switch (t) {
                case a:
                    t = HelperFctns.nextType21();
                    break;
                case b:
                    t = HelperFctns.nextType21();
                    break;
                case c:
                    t = HelperFctns.nextType21();
                    break;
                case d:
                    t = HelperFctns.nextType21();
                    break;
                case e:
                    t = HelperFctns.nextType21();
                    break;
                case f:
                    t = HelperFctns.nextType21();
                    break;
                case g:
                    t = HelperFctns.nextType21();
                    break;
                case h:
                    t = HelperFctns.nextType21();
                    break;
                case i:
                    t = HelperFctns.nextType21();
                    break;
                case j:
                    t = HelperFctns.nextType21();
                    break;
                case k:
                    t = HelperFctns.nextType21();
                    break;
                case l:
                    t = HelperFctns.nextType21();
                    break;
                case m:
                    t = HelperFctns.nextType21();
                    break;
                case n:
                    t = HelperFctns.nextType21();
                    break;
                case o:
                    t = HelperFctns.nextType21();
                    break;
                case p:
                    t = HelperFctns.nextType21();
                    break;
                case q:
                    t = HelperFctns.nextType21();
                    break;
                case r:
                    t = HelperFctns.nextType21();
                    break;
                case s:
                    t = HelperFctns.nextType21();
                    break;
                case t:
                    t = HelperFctns.nextType21();
                    break;
                case u:
                    t = HelperFctns.nextType21();
                    break;
                default:
                    t = HelperFctns.nextType21();
                    break;
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }
}
