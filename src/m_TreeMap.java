import java.util.TreeMap;
import java.util.Map;
import java.util.Random;

public class m_TreeMap implements I_CollectionExpFctns {
    Random r = new Random();

    @Override
    public void insert_int(int num, int repeats) {
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            Map<Integer, Integer> c = new TreeMap();
            for (int j = 0; j < num; ++j) {
                c.put(j, r.nextInt()); //random numbers to prevent optimisation
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void insert_dbl(int num, int repeats) {
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            Map<Integer, Double> c = new TreeMap();
            for (int j = 0; j < num; ++j) {
                c.put(j, r.nextDouble());
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void insert_str(int num, int repeats) {
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            Map<Integer, String> c = new TreeMap();
            for (int j = 0; j < num; ++j) {
                c.put(j, HelperFctns.nextString());
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void iterate_loop_int(int num, int repeats) {
        Map<Integer, Integer> c = new TreeMap<>();
        for (int j = 0; j < num; ++j) {
            c.put(j, r.nextInt());
        }
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            for (int j = 0; j < c.size(); ++j) {
                c.put(j, c.get(j) + 1);
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void iterate_loop_dbl(int num, int repeats) {
        Map<Integer, Double> c = new TreeMap<>();
        for (int j = 0; j < num; ++j) {
            c.put(j, r.nextDouble());
        }
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            for (int j = 0; j < c.size(); ++j) {
                c.put(j, c.get(j) + 1.0);
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void iterate_loop_str(int num, int repeats) {
        Map<Integer, String> c = new TreeMap<>();
        for (int j = 0; j < num; ++j) {
            c.put(j, HelperFctns.nextString());
        }
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            for (int j = 0; j < c.size(); ++j) {
                c.put(j, c.get(j) + "");
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void iterate_iter_int(int num, int repeats) {
        System.out.println("No iterators for Maps.");
    }

    @Override
    public void iterate_iter_dbl(int num, int repeats) {
        System.out.println("No iterators for Maps.");
    }

    @Override
    public void iterate_iter_str(int num, int repeats) {
        System.out.println("No iterators for Maps.");
    }


    @Override
    public void randr_int(int num, int repeats) {
        Map<Integer, Integer> c = new TreeMap();
        for (int j = 0; j < num; ++j) {
            c.put(j, r.nextInt());
        }
        int x;
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            x = c.get(r.nextInt(num));
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    public void randr_dbl(int num, int repeats) {
        Map<Integer, Double> c = new TreeMap();
        for (int j = 0; j < num; ++j) {
            c.put(j, r.nextDouble());
        }
        double x;
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            x = c.get(r.nextInt(num));
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void randr_str(int num, int repeats) {
        Map<Integer, String> c = new TreeMap();
        for (int j = 0; j < num; ++j) {
            c.put(j, HelperFctns.nextString());
        }
        String x;
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            x = c.get(r.nextInt(num));
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void randw_int(int num, int repeats) {
        Map<Integer, Integer> c = new TreeMap();
        for (int j = 0; j < num; ++j) {
            c.put(j, r.nextInt());
        }
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            c.put(r.nextInt(num), r.nextInt());
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void randw_dbl(int num, int repeats) {
        Map<Integer, Double> c = new TreeMap();
        for (int j = 0; j < num; ++j) {
            c.put(j, r.nextDouble());
        }
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            c.put(r.nextInt(num), r.nextDouble());
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void randw_str(int num, int repeats) {
        Map<Integer, String> c = new TreeMap();
        for (int j = 0; j < num; ++j) {
            c.put(j, HelperFctns.nextString());
        }
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            c.put(r.nextInt(num), HelperFctns.nextString());
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void contains_int(int num, int repeats) {
        Map<Integer, Integer> c = new TreeMap<>();
        for (int j = 0; j < num; ++j) {
            c.put(j, r.nextInt());
        }
        HelperFctns.setContainsInt(c);
        boolean x;
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            x = c.containsValue(HelperFctns.nextContainsInt());
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void contains_dbl(int num, int repeats) {
        Map<Integer, Double> c = new TreeMap<>();
        for (int j = 0; j < num; ++j) {
            c.put(j, r.nextDouble());
        }
        HelperFctns.setContainsDbl(c);
        boolean x;
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            x = c.containsValue(HelperFctns.nextContainsDbl());
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void contains_str(int num, int repeats) {
        Map<Integer, String> c = new TreeMap<>();
        for (int j = 0; j < num; ++j) {
            c.put(j, HelperFctns.nextString());
        }
        HelperFctns.setContainsStr(c);
        boolean x;
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            x = c.containsValue(HelperFctns.nextContainsStr());
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }
}
