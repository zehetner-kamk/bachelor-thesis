import java.nio.charset.StandardCharsets;
import java.util.*;

public class HelperFctns {
    private static List<Integer> containsInt = new ArrayList<>();
    private static List<Double> containsDbl = new ArrayList<>();
    private static List<String> containsStr = new ArrayList<>();


    public static enum Type6 {a, b, c, d, e, f}

    public static enum Type21 {a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u}

    private static Random r = new Random();


    public static String nextString() {
        byte[] array = new byte[r.nextInt(41) + 10]; //strings are 10-50 chars long
        r.nextBytes(array);
        return new String(array, StandardCharsets.UTF_8);
    }

    public static Type6 nextType6() {
        char c = (char) (r.nextInt(6) + 'a');
        return Type6.valueOf(String.valueOf(c));
    }

    public static Type21 nextType21() {
        char c = (char) (r.nextInt(21) + 'a');
        return Type21.valueOf(String.valueOf(c));
    }


    public static int nextContainsInt() {
        int x = containsInt.get(r.nextInt(containsInt.size()));
        if (r.nextInt(10) == 0) x = r.nextInt(); //very high chance that this item is not in list
        return x;
    }

    public static double nextContainsDbl() {
        double x = containsDbl.get(r.nextInt(containsDbl.size()));
        if (r.nextInt(10) == 0) x = r.nextDouble();
        return x;
    }

    public static String nextContainsStr() {
        String x = containsStr.get(r.nextInt(containsStr.size()));
        if (r.nextInt(10) == 0) x = nextString();
        return x;
    }


    public static void setContainsInt(List<Integer> c) {
        containsInt.addAll(c); //deep copy
    }

    public static void setContainsDbl(List<Double> c) {
        containsDbl.addAll(c);
    }

    public static void setContainsStr(List<String> c) {
        containsStr.addAll(c);
    }


    public static void setContainsInt(Map<Integer, Integer> c) {
        containsInt.addAll(c.values()); //deep copy
    }

    public static void setContainsDbl(Map<Integer, Double> c) {
        containsDbl.addAll(c.values());
    }

    public static void setContainsStr(Map<Integer, String> c) {
        containsStr.addAll(c.values());
    }

    
    public static void setContainsInt(Set<Integer> c) {
        containsInt.addAll(c); //deep copy
    }

    public static void setContainsDbl(Set<Double> c) {
        containsDbl.addAll(c);
    }

    public static void setContainsStr(Set<String> c) {
        containsStr.addAll(c);
    }
}

