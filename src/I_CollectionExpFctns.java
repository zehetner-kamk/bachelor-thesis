public interface I_CollectionExpFctns { //collection implementation experiment functions
    void insert_int(int num, int repeats); //50-500-5000, 100000-25000-5000

    void insert_dbl(int num, int repeats);

    void insert_str(int num, int repeats);

    void iterate_loop_int(int num, int repeats);

    void iterate_loop_dbl(int num, int repeats);

    void iterate_loop_str(int num, int repeats);

    void iterate_iter_int(int num, int repeats);

    void iterate_iter_dbl(int num, int repeats);

    void iterate_iter_str(int num, int repeats);

    void randr_int(int num, int repeats);

    void randr_dbl(int num, int repeats);

    void randr_str(int num, int repeats);

    void randw_int(int num, int repeats);

    void randw_dbl(int num, int repeats);

    void randw_str(int num, int repeats);

    void contains_int(int num, int repeats);

    void contains_dbl(int num, int repeats);

    void contains_str(int num, int repeats);
}
