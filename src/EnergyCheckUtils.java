import database.ResultDO;
import database.ResultDao;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EnergyCheckUtils {
    public native static int scale(int freq);

    public native static int[] freqAvailable();

    public native static double[] GetPackagePowerSpec();

    public native static double[] GetDramPowerSpec();

    public native static void SetPackagePowerLimit(int socketId, int level, double costomPower);

    public native static void SetPackageTimeWindowLimit(int socketId, int level, double costomTimeWin);

    public native static void SetDramTimeWindowLimit(int socketId, int level, double costomTimeWin);

    public native static void SetDramPowerLimit(int socketId, int level, double costomPower);

    public native static int ProfileInit();

    public native static int GetSocketNum();

    public native static String EnergyStatCheck();

    public native static void ProfileDealloc();

    public native static void SetPowerLimit(int ENABLE);

    public static int wraparoundValue;
    public static int socketNum;

    static {
        System.setProperty("java.library.path", System.getProperty("user.dir"));
        //System.out.println(System.getProperty("user.dir"));
        try {
            Field fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
            fieldSysPath.setAccessible(true);
            fieldSysPath.set(null, null);
        } catch (Exception e) {
        }

        System.loadLibrary("CPUScaler");
        wraparoundValue = ProfileInit();
        socketNum = GetSocketNum();
    }

    /**
     * @return an array of current energy information.
     * The first entry is: Dram/uncore gpu energy(depends on the cpu architecture.
     * The second entry is: CPU energy
     * The third entry is: Package energy
     */

    public static double[] getEnergyStats() {
        socketNum = GetSocketNum();
        String EnergyInfo = EnergyStatCheck();
        //System.out.println(EnergyInfo);
        /*One Socket*/
        if (socketNum == 1) {
            double[] stats = new double[3];
            String[] energy = EnergyInfo.split("#");

            stats[0] = Double.parseDouble(energy[0].replace(",", ".")); //convert german to english numbers
            stats[1] = Double.parseDouble(energy[1].replace(",", "."));
            stats[2] = Double.parseDouble(energy[2].replace(",", "."));

            return stats;

        } else {
            /*Multiple sockets*/
            String[] perSockEner = EnergyInfo.split("@");
            double[] stats = new double[3 * socketNum];
            int count = 0;


            for (int i = 0; i < perSockEner.length; i++) {
                String[] energy = perSockEner[i].split("#");
                for (int j = 0; j < energy.length; j++) {
                    count = i * 3 + j;    //accumulative count
                    stats[count] = Double.parseDouble(energy[j]);
                }
            }
            return stats;
        }

    }
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


    private static ResultDao resultDao = new ResultDao();
    public static int numnum;
    public static int repeats;
    public static String method;
    public static int iteration;


    public static void main(String[] args) {
        I_CollectionExpFctns arrayList = new l_ArrayList();
        I_CollectionExpFctns copyOnWriteArrayList = new l_CopyOnWriteArrayList();
        I_CollectionExpFctns linkedList = new l_LinkedList();

        I_CollectionExpFctns concurrentHashMap = new m_ConcurrentHashMap();
        I_CollectionExpFctns concurrentSkipListMap = new m_ConcurrentSkipListMap();
        I_CollectionExpFctns hashMap = new m_HashMap();
        I_CollectionExpFctns linkedHashMap = new m_LinkedHashMap();
        I_CollectionExpFctns treeMap = new m_TreeMap();

        I_CollectionExpFctns concurrentSkipListSet = new s_ConcurrentSkipListSet();
        I_CollectionExpFctns copyOnWriteArraySet = new s_CopyOnWriteArraySet();
        I_CollectionExpFctns hashSet = new s_HashSet();
        I_CollectionExpFctns linkedHashSet = new s_LinkedHashSet();
        I_CollectionExpFctns treeSet = new s_TreeSet();

        I_StructureExpFctns structure5 = new z_Structure5();
        I_StructureExpFctns structure20 = new z_Structure20();


        I_CollectionExpFctns coll = null; //collection that is measured
        switch (args[0]) {
            case "arrayList":
                coll = arrayList;
                break;
            case "copyOnWriteArrayList":
                coll = copyOnWriteArrayList;
                break;
            case "linkedList":
                coll = linkedList;
                break;
            case "concurrentHashMap":
                coll = concurrentHashMap;
                break;
            case "concurrentSkipListMap":
                coll = concurrentSkipListMap;
                break;
            case "hashMap":
                coll = hashMap;
                break;
            case "linkedHashMap":
                coll = linkedHashMap;
                break;
            case "treeMap":
                coll = treeMap;
                break;
            case "concurrentSkipListSet":
                coll = concurrentSkipListSet;
                break;
            case "copyOnWriteArraySet":
                coll = copyOnWriteArraySet;
                break;
            case "hashSet":
                coll = hashSet;
                break;
            case "linkedHashSet":
                coll = linkedHashSet;
                break;
            case "treeSet":
                coll = treeSet;
                break;
        }

        //Function calls here.
        method = args[1];
        numnum = Integer.parseInt(args[2]);
        repeats = Integer.parseInt(args[3]);
        for (iteration = 0; iteration < 20; ++iteration) {
            switch (method) {
                case "insert_int":
                    coll.insert_int(numnum, repeats);
                    break;
                case "insert_dbl":
                    coll.insert_dbl(numnum, repeats);
                    break;
                case "insert_str":
                    coll.insert_str(numnum, repeats);
                    break;
                case "iterate_loop_int":
                    coll.iterate_loop_int(numnum, repeats);
                    break;
                case "iterate_loop_dbl":
                    coll.iterate_loop_dbl(numnum, repeats);
                    break;
                case "iterate_loop_str":
                    coll.iterate_loop_str(numnum, repeats);
                    break;
                case "iterate_iter_int":
                    coll.iterate_iter_int(numnum, repeats);
                    break;
                case "iterate_iter_dbl":
                    coll.iterate_iter_dbl(numnum, repeats);
                    break;
                case "iterate_iter_str":
                    coll.iterate_iter_str(numnum, repeats);
                    break;
                case "randr_int":
                    coll.randr_int(numnum, repeats);
                    break;
                case "randr_dbl":
                    coll.randr_dbl(numnum, repeats);
                    break;
                case "randr_str":
                    coll.randr_str(numnum, repeats);
                    break;
                case "randw_int":
                    coll.randw_int(numnum, repeats);
                    break;
                case "randw_dbl":
                    coll.randw_dbl(numnum, repeats);
                    break;
                case "randw_str":
                    coll.randw_str(numnum, repeats);
                    break;
                case "contains_int":
                    coll.contains_int(numnum, repeats);
                    break;
                case "contains_dbl":
                    coll.contains_dbl(numnum, repeats);
                    break;
                case "contains_str":
                    coll.contains_str(numnum, repeats);
                    break;
            }
        }

        System.out.println("DONE.");
        ProfileDealloc();
    }


    public static void saveEnergyConsumption(double[] before, double[] after, double duration) {
        double power = after[0] - before[0] + after[1] - before[1] + after[2] - before[2];

        printEnergyConsumption(duration, power);
        if (socketNum > 1) { //idk what socketNum is, but it has been 1 all the time
            System.out.println("SocketNum > 1!");
            return;
        }

        StackTraceElement callingMethod = new Throwable().getStackTrace()[new Throwable().getStackTrace().length - 2];
        String implementationName = callingMethod.getClassName();
        String methodName = callingMethod.getMethodName(); //name of method that called saveEnergyConsumption()
        String collectionName = "";
        switch (implementationName.substring(0, 1)) {
            case "l":
                collectionName = "List";
                break;
            case "m":
                collectionName = "Map";
                break;
            case "s":
                collectionName = "Set";
                break;
            case "z":
                collectionName = "Structure";
                break;
            default:
                System.out.println("No matching collection");
        }
        ResultDO result = new ResultDO(collectionName, implementationName.substring(2), methodName.substring(0, methodName.length() - 4), methodName.substring(methodName.length() - 3), numnum, iteration, duration, power);
        resultDao.insert(result);
    }

    private static void printEnergyConsumption(double duration, double energy) {
        for (int i = 0; i < socketNum; ++i) {
            System.out.println("Iteration: " + iteration + ", Duration: " + duration + " s, Energy: " + Math.round(energy) + " Ws");
            //System.out.println("Power consumption of dram: " + (after[0] - before[0]) / duration + " energy consumption of cpu: " + (after[1] - before[1]) / duration + " energy consumption of package: " + (after[2] - before[2]) / duration);
        }
    }
}
