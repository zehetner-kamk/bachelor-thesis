import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

public class s_LinkedHashSet implements I_CollectionExpFctns {
    Random r = new Random();

    @Override
    public void insert_int(int num, int repeats) {
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            Set<Integer> c = new LinkedHashSet();
            for (int j = 0; j < num; ++j) {
                c.add(r.nextInt()); //random numbers to prevent optimisation
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void insert_dbl(int num, int repeats) {
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            Set<Double> c = new LinkedHashSet();
            for (int j = 0; j < num; ++j) {
                c.add(r.nextDouble());
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void insert_str(int num, int repeats) {
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            Set<String> c = new LinkedHashSet();
            for (int j = 0; j < num; ++j) {
                c.add(HelperFctns.nextString());
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }


    @Override
    public void iterate_loop_int(int num, int repeats) {
        System.out.println("For loop traversion not available for Sets.");
    }

    @Override
    public void iterate_loop_dbl(int num, int repeats) {
        System.out.println("For loop traversion not available for Sets.");

    }

    @Override
    public void iterate_loop_str(int num, int repeats) {
        System.out.println("For loop traversion not available for Sets.");
    }

    @Override
    public void iterate_iter_int(int num, int repeats) {
        Set<Integer> c = new LinkedHashSet();
        for (int j = 0; j < num; ++j) {
            c.add(r.nextInt());
        }
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            for (int x : c) {
                ++x;
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void iterate_iter_dbl(int num, int repeats) {
        Set<Double> c = new LinkedHashSet();
        for (int j = 0; j < num; ++j) {
            c.add(r.nextDouble());
        }
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            for (double x : c) {
                ++x;
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void iterate_iter_str(int num, int repeats) {
        Set<String> c = new LinkedHashSet();
        for (int j = 0; j < num; ++j) {
            c.add(HelperFctns.nextString());
        }
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            for (String x : c) {
                x += "";
            }
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void randr_int(int num, int repeats) {
        System.out.println("No random read for Sets.");
    }

    public void randr_dbl(int num, int repeats) {
        System.out.println("No random read for Sets.");

    }

    @Override
    public void randr_str(int num, int repeats) {
        System.out.println("No random read for Sets.");
    }

    @Override
    public void randw_int(int num, int repeats) {
        System.out.println("No random write for Sets.");

    }

    @Override
    public void randw_dbl(int num, int repeats) {
        System.out.println("No random write for Sets.");
    }

    @Override
    public void randw_str(int num, int repeats) {
        System.out.println("No random write for Sets.");
    }

    @Override
    public void contains_int(int num, int repeats) {
        Set<Integer> c = new LinkedHashSet<>();
        for (int j = 0; j < num; ++j) {
            c.add(r.nextInt());
        }
        HelperFctns.setContainsInt(c);
        boolean x;
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            x = c.contains(HelperFctns.nextContainsInt());
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void contains_dbl(int num, int repeats) {
        Set<Double> c = new LinkedHashSet<>();
        for (int j = 0; j < num; ++j) {
            c.add(r.nextDouble());
        }
        HelperFctns.setContainsDbl(c);
        boolean x;
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            x = c.contains(HelperFctns.nextContainsDbl());
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

    @Override
    public void contains_str(int num, int repeats) {
        Set<String> c = new LinkedHashSet<>();
        for (int j = 0; j < num; ++j) {
            c.add(HelperFctns.nextString());
        }
        HelperFctns.setContainsStr(c);
        boolean x;
        long startTime = System.currentTimeMillis();
        double[] before = EnergyCheckUtils.getEnergyStats();


        for (int i = 0; i < repeats; ++i) {
            x = c.contains(HelperFctns.nextContainsStr());
        }


        double[] after = EnergyCheckUtils.getEnergyStats();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        EnergyCheckUtils.saveEnergyConsumption(before, after, duration);
    }

}
