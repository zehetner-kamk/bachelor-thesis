import os
import time

# Already done: "arrayList", "copyOnWriteArrayList", "linkedList","concurrentHashMap", "concurrentSkipListMap","hashMap","linkedHashMap","treeMap","concurrentSkipListSet","copyOnWriteArraySet","hashSet","linkedHashSet",
# , "contains_int", "contains_dbl", "contains_str"
impls = ["treeSet"]
methods = ["insert_int", "insert_dbl", "insert_str", "iterate_loop_int", "iterate_loop_dbl", "iterate_loop_str", "iterate_iter_int", "iterate_iter_dbl",
           "iterate_iter_str", "randr_int", "randr_dbl", "randr_str", "randw_int", "randw_dbl", "randw_str"]
nums = [50, 500, 5000]
repeats = [100000, 25000, 5000]

for impl in impls:
    for method in methods:
        for i in range(0, 3):
            print("\n\n" + impl + " " + method + " " + str(nums[i]))

            file = open("run_auto_experiment", "r")
            if(file.readline() == "1"):  # stop program if needed
                time.sleep(5)  # let system cool down
                os.system("sudo /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java -jar out/artifacts/jRapl.jar " +
                          impl + " " + method + " " + str(nums[i]) + " " + str(repeats[i]))
            file.close()
