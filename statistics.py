import sqlite3
import numpy as np
from numpy.core.defchararray import array
from numpy.core.fromnumeric import mean
import pandas
from matplotlib import pyplot as plot


def make_array(result):
    a = []
    for i in result:
        a.append(i[0])
    return a


conn = sqlite3.connect(r"results_new.db")
cursor = conn.cursor()


def numbers():
    duration = make_array(cursor.execute("SELECT duration FROM r"))
    power = make_array(cursor.execute("SELECT power FROM r"))

    print(np.mean(duration), np.min(duration), np.max(duration), np.median(duration))
    print(np.mean(power), np.min(power), np.max(power), np.median(power))


def boxplot_scatterplot():
    duration = make_array(cursor.execute("SELECT duration FROM r"))
    power = make_array(cursor.execute("SELECT power FROM r"))

    fig, axes = plot.subplots(2, 2)
    axes[0, 0].boxplot(duration, labels=["Dauer (sek)"])
    axes[0, 1].boxplot(power, labels=["Energieverbrauch (Ws)"])

    plot.scatter(duration, power)
    plot.xlabel("Dauer (sek)")
    plot.ylabel("Energieverbrauch (Ws)")


def round_and_format(x):
    return str(np.round(x, 2)).replace(".", ",")


def stddev_by_method():
    insert = make_array(cursor.execute("SELECT duration FROM r WHERE method='insert'"))
    iterate_loop = make_array(cursor.execute("SELECT duration FROM r WHERE method='iterate_loop'"))
    iterate_iter = make_array(cursor.execute("SELECT duration FROM r WHERE method='iterate_iter'"))
    randr = make_array(cursor.execute("SELECT duration FROM r WHERE method='randr'"))
    randw = make_array(cursor.execute("SELECT duration FROM r WHERE method='randw'"))

    print(insert[0])
    fig, grid = plot.subplots(2, 3)
    grid[0, 0].boxplot(insert, labels=["Einfügen: " + round_and_format(np.std(insert))])
    grid[0, 1].boxplot(iterate_loop, labels=["Schleife: " + round_and_format(np.std(iterate_loop))])
    grid[0, 2].boxplot(iterate_iter, labels=["Iterator: " + round_and_format(np.std(iterate_iter))])
    grid[1, 0].boxplot(randr, labels=["Auslesen: " + round_and_format(np.std(randw))])
    grid[1, 1].boxplot(randw, labels=["Schreiben: " + round_and_format(np.std(randr))])
    grid[0, 0].set_ylabel("Ws")
    grid[1, 0].set_ylabel("Ws")


def line_avg_by_iteration():
    data = []
    for i in range(20):
        data.append(make_array(cursor.execute("SELECT power FROM r WHERE iteration="+str(i))))
    idx = [i for i in range(20)]
    means = [np.mean(data[i]) for i in range(20)]
    medians = [np.median(data[i]) for i in range(20)]

    fig, grid = plot.subplots(2, 2)
    grid[0, 0].scatter(idx, means)
    grid[0, 1].scatter(idx, medians)
    grid[0, 0].plot(idx, means)
    grid[0, 1].plot(idx, medians)
    grid[0, 0].set_xlabel("Wiederholung")
    grid[0, 0].set_ylabel("Ws")
    grid[0, 0].set_title("Mittel")
    grid[0, 1].set_title("Median")
    print(means[0])
    print(np.mean(means[1:20]))


def get_max(data):
    maxs = []
    for i in data:
        for j in i:
            x = [-1 if k == None else k for k in j]
            maxs.append(max(x))
    return max(maxs)


def get_min(data):
    mins = []
    for i in data:
        for j in i:
            x = [-1 if k == None else k for k in j]
            mins.append(min(x))
    return min(mins)


def line_avg_by_measurement(coll, method):
    # SELECT impl, num, AVG(power) FROM r WHERE coll='List'AND method='insert' AND datatype='int' GROUP BY impl, num
    impls = make_array(cursor.execute("SELECT impl FROM r WHERE coll=? GROUP BY impl", [coll]))  # all impls of this collection
    nums = [50, 500, 5000]
    colours = ["blue", "orange", "green", "red", "violet", "brown", "pink", "grey", "yellow", "turquoise"]

    data = []  # layers: data[dt][impl][num]
    for dt in ["int", "dbl", "str"]:
        x = []
        for im in impls:
            y = []
            for numnum in nums:
                y.append(make_array(cursor.execute(
                    "SELECT AVG(power) FROM r WHERE method=? AND datatype=? AND impl=? AND num=?", [method, dt, im, numnum]))[0])
            x.append(y)
        data.append(x)

    fig, grid = plot.subplots(2, 3)
    y_up = get_max(data)*1.05
    y_lo = get_min(data) - get_max(data)*0.05
    #grid[0, 0].set_ylim(y_lo, y_up)
    #grid[0, 1].set_ylim(y_lo, y_up)
    #grid[0, 2].set_ylim(y_lo, y_up)
    #grid[0, 2].set_xlim(-150, nums[len(nums)-1]+200)
    #grid[0, 0].set_xlim(-150, nums[len(nums)-1]+200)
    #grid[0, 1].set_xlim(-150, nums[len(nums)-1]+200)

    str_nums = [str(num) for num in nums for impl in impls]
    print(str_nums)
    plot.subplots_adjust(top=0.7, right=0.98)
    for i in range(len(nums)):
        grid[0, 0].bar(nums, data[0][i])
        grid[0, 1].bar(nums, data[1][i])
        grid[0, 2].bar(nums, data[2][i])
        #grid[0, 0].plot(nums, data[0][i])
        #grid[0, 1].plot(nums, data[1][i])
        #grid[0, 2].plot(nums, data[2][i])
        print(impls[i]+": " + colours[i])
    grid[0, 1].set_xlabel("Anz. Elem.")
    grid[0, 0].set_ylabel("Ws")
    grid[0, 0].set_title("Integer")
    grid[0, 1].set_title("Double")
    grid[0, 2].set_title("String")


def line_avg_all():
    for coll in ["List", "Map", "Set"]:
        for method in ["insert", "iterate_loop", "iterate_iter", "randr", "randw", "contains"]:
            print("\n"+coll + " " + method)
            line_avg_by_measurement(coll, method)
            plot.show()


def line_avg_by_measurement_decide(coll):
    meths = make_array(cursor.execute("SELECT method FROM r WHERE coll=? GROUP BY method", [coll]))  # all methods of this collection
    print(meths)
    nums = [5, 20]
    colours = ["blue", "orange", "green", "red", "violet", "brown", "pink", "grey", "yellow", "turquoise"]

    data = []  # layers: data[dt][meth][num]
    for dt in ["int", "str", "enm"]:
        x = []
        for me in meths:
            y = []
            for numnum in nums:
                y.append(make_array(cursor.execute(
                    "SELECT AVG(power) FROM r WHERE method=? AND datatype=? AND num=?", [me, dt, numnum]))[0])
            x.append(y)
        data.append(x)

    fig, grid = plot.subplots(2, 3)
    y_up = get_max(data)*1.05
    y_lo = get_min(data) - get_max(data)*0.05
    grid[0, 0].set_ylim(y_lo, y_up)
    grid[0, 1].set_ylim(y_lo, y_up)
    grid[0, 2].set_ylim(y_lo, y_up)
    grid[0, 0].set_xlim(0, nums[len(nums)-1]+5)
    grid[0, 1].set_xlim(0, nums[len(nums)-1]+5)
    grid[0, 2].set_xlim(0, nums[len(nums)-1]+5)
    plot.subplots_adjust(top=0.7, right=0.98)
    for i in range(len(meths)):
        grid[0, 0].scatter(nums, data[0][i])
        grid[0, 1].scatter(nums, data[1][i])
        grid[0, 2].scatter(nums, data[2][i])
        grid[0, 0].plot(nums, data[0][i])
        grid[0, 1].plot(nums, data[1][i])
        grid[0, 2].plot(nums, data[2][i])
        print(meths[i]+": " + colours[i])
    grid[0, 1].set_xlabel("Anz. Elem.")
    grid[0, 0].set_ylabel("Ws")
    grid[0, 0].set_title("Integer")
    grid[0, 1].set_title("String")
    grid[0, 2].set_title("Enum")


def line_avg_by_measurement_traverse():
    meths = ["iterate_loop", "iterate_iter"]
    nums = [50, 500, 5000]
    colours = ["blue", "orange", "green", "red", "violet", "brown", "pink", "grey", "yellow", "turquoise"]

    data = []  # layers: data[dt][meth][num]
    for dt in ["int", "dbl", "str"]:
        x = []
        for me in meths:
            y = []
            for numnum in nums:
                y.append(make_array(cursor.execute(
                    "SELECT AVG(power) FROM r WHERE method=? AND datatype=? AND num=?", [me, dt, numnum]))[0])
            x.append(y)
        data.append(x)

    fig, grid = plot.subplots(2, 3)
    y_up = get_max(data)*1.05
    y_lo = get_min(data) - get_max(data)*0.05
    grid[0, 0].set_ylim(y_lo, y_up)
    grid[0, 1].set_ylim(y_lo, y_up)
    grid[0, 2].set_ylim(y_lo, y_up)
    grid[0, 0].set_xlim(-150, nums[len(nums)-1]+200)
    grid[0, 1].set_xlim(-150, nums[len(nums)-1]+200)
    grid[0, 2].set_xlim(-150, nums[len(nums)-1]+200)
    plot.subplots_adjust(top=0.7, right=0.98)
    for i in range(len(meths)):
        grid[0, 0].scatter(nums, data[0][i])
        grid[0, 1].scatter(nums, data[1][i])
        grid[0, 2].scatter(nums, data[2][i])
        grid[0, 0].plot(nums, data[0][i])
        grid[0, 1].plot(nums, data[1][i])
        grid[0, 2].plot(nums, data[2][i])
        print(meths[i]+": " + colours[i])
    grid[0, 1].set_xlabel("Anz. Elem.")
    grid[0, 0].set_ylabel("Ws")
    grid[0, 0].set_title("Integer")
    grid[0, 1].set_title("Double")
    grid[0, 2].set_title("String")


# Function calls here
#line_avg_by_measurement("List", "randw")

plot.show()
cursor.close()
conn.close()
